﻿using System;
using System.Configuration;
using System.Threading;

namespace OtusHomeWork_Server
{
    class Program
    {
        static ServerObject server; // сервер
        static int port;
        static Thread listenThread; // потока для прослушивания
        static void Main(string[] args)
        {
            try
            {
                ReadSettings();

                server = new ServerObject(port);

                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }

        static void ReadSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    Console.WriteLine("AppSettings is empty.");
                }
                else
                {
                    if (appSettings["port"] != null)
                        port = Int32.Parse(appSettings["port"]);
                }
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }
    }
}
