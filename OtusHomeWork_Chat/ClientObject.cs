﻿using ClassLibrary;
using System;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace OtusHomeWork_Server
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream stream { get; private set; }
        protected BinaryFormatter bFormatter;

        private string userName;
        private TcpClient client;
        private ServerObject server;

        
        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            bFormatter = new BinaryFormatter();
            serverObject.AddConnection(this);
        }

        /// <summary>
        /// Обработка сообщенией о клиента
        /// </summary>
        public void Process()
        {
            try
            {
                stream = client.GetStream();
                // получаем имя пользователя
                string message = GetMessage();
                userName = message;

                message = userName + " вошел в чат";

                // посылаем сообщение о входе в чат всем подключенным пользователям
                server.BroadcastMessage(message, this.Id);
                Console.WriteLine(message);

                // в бесконечном цикле получаем сообщения от клиента
                do
                {
                    message = GetMessage();
                    message = String.Format("{0}: {1}", userName, message);
                    Console.WriteLine(message);
                    server.BroadcastMessage(message, this.Id);
                } while (client.Connected);
            }
            catch (SerializationException e)
            {
                server.ReturnErrorMessage(e.Message, this.Id);

                SendMessageAfterErrorForAll();
            }
            
            catch (Exception e)
            {
                if(client.Connected)
                    server.ReturnErrorMessage(e.Message, this.Id);

                SendMessageAfterErrorForAll();
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        /// <summary>
        /// После ошибки, клиент отключен, и сообщаем  остальным что он покуинул чат
        /// </summary>
        private void SendMessageAfterErrorForAll()
        {
            string messageForOtherClient = String.Format("{0}: покинул чат", userName);
            Console.WriteLine(messageForOtherClient);
            server.BroadcastMessage(messageForOtherClient, this.Id);
        }

        /// <summary>
        /// чтение входящего сообщения и преобразование в строку
        /// </summary>
        /// <returns></returns>
        private string GetMessage()
        {
            Message messageObj = (Message)this.bFormatter.Deserialize(stream);

            return messageObj.Text;
        }

        /// <summary>
        /// закрытие подключения
        /// </summary>
        protected internal void Close()
        {
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }
}