﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace OtusHomeWork_Server
{
    public class ServerObject
    {
        private static TcpListener tcpListener;
        private List<ClientObject> clients = new List<ClientObject>();         
        private BinaryFormatter bFormatter = new BinaryFormatter();
        private int port;

        public ServerObject(int port)
        {
            this.port = port;
        }

        /// <summary>
        /// Добавление клиента в список подключений
        /// </summary>
        /// <param name="clientObject"></param>
        protected internal void AddConnection(ClientObject clientObject)
        {
            clients.Add(clientObject);
        }

        /// <summary>
        /// Удаление клиента из списока подключений
        /// </summary>
        /// <param name="id"></param>
        protected internal void RemoveConnection(string id)
        {
            ClientObject client = clients.FirstOrDefault(c => c.Id == id);

            if (client != null)
                clients.Remove(client);
        }

        /// <summary>
        /// прослушивание входящих подключений
        /// </summary>
        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, port);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        /// <summary>
        /// трансляция сообщения подключенным клиентам
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        protected internal void BroadcastMessage(string message, string id)
        {
            Message messageObj = new Message();
            messageObj.Text = message;
            messageObj.Status = Status.Ok;

            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != id)
                {
                    //сериализация и передача данных
                    bFormatter.Serialize(clients[i].stream, messageObj);
                }
            }
        }

        
        /// <summary>
        /// Возврат ошибки клиенту
        /// </summary>
        /// <param name="errorText"></param>
        /// <param name="id"></param>
        protected internal void ReturnErrorMessage(string errorText, string id)
        {
            var clientStream = clients.First(c => c.Id == id).stream;
            if (clientStream.DataAvailable)
            {
                Message messageObj = new Message();
                messageObj.Text = errorText;
                messageObj.Status = Status.Error;

                //сериализация и передача данных
                bFormatter.Serialize(clientStream, messageObj);
            }
        }

        /// <summary>
        /// отключение всех клиентов
        /// </summary>
        protected internal void Disconnect()
        {
            //остановка сервера
            tcpListener.Stop(); 

            for (int i = 0; i < clients.Count; i++)
            {
                //отключение клиента
                clients[i].Close();
            }

            //завершение процесса
            Environment.Exit(0); 
        }
    }
}
