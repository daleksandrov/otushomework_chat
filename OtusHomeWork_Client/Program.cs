﻿using ClassLibrary;
using System;
using System.Configuration;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace OtusHomeWork_Client
{
    class Program
    {
        private static string userName;
        private static string host;
        private static int port;
        private static TcpClient client;
        private static NetworkStream stream;
        private static BinaryFormatter bFormatter;

        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            bFormatter = new BinaryFormatter();

            try
            {
                ReadSettings();

                //подключение клиента
                client.Connect(host, port);

                // получаем поток
                stream = client.GetStream(); 

                Message messageObj = new Message();
                messageObj.Text = userName;

                //сериализация и передача данных
                bFormatter.Serialize(stream, messageObj);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока


                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }

        static void ReadSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    Console.WriteLine("AppSettings is empty.");
                }
                else
                {
                    host = appSettings["host"];

                    if(appSettings["port"] != null)
                        port = Int32.Parse(appSettings["port"]);
                }
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }

        /// <summary>
        ///  отправка сообщений
        /// </summary>
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                Message messageObj = new Message();

                string msg = Console.ReadLine();
                if (msg == "q" || msg == "exit")
                {
                    Disconnect();
                }
                else
                {
                    messageObj.Text = msg;
                    messageObj.Status = Status.Ok;

                    bFormatter.Serialize(stream, messageObj);//сериализация и передача данных
                }                                               
            }
        }

        /// <summary>
        ///  Получение сообщений
        /// </summary>
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    Message messageObj = (Message)bFormatter.Deserialize(stream);

                    if (messageObj.Status == Status.Ok)
                    {
                        //вывод сообщения
                        Console.WriteLine(messageObj.Text);
                    }
                    else if (messageObj.Status == Status.Error)
                    {
                        Console.WriteLine(messageObj.Text);
                        Console.ReadLine();
                        Disconnect();
                    }
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); 
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        /// <summary>
        /// Закрытие потока и отключение соединения
        /// </summary>
        static void Disconnect()
        {

            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
            Environment.Exit(0);
        }

    }
}
