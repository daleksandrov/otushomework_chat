﻿using System;

namespace ClassLibrary
{
    [Serializable]
    public class Message
    {     
        /// <summary>
        /// Статус сообщения
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }
    }
}
